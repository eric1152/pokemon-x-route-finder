#!/usr/bin/env python

import sqlite3

conn = sqlite3.connect('../pokemon.db')
c = conn.cursor();

c.execute("CREATE TABLE IF NOT EXISTS pokemon (name text NOT NULL, number text NOT NULL, routes text NOT NULL, ptype text, eggGroup text, captured boolean, PRIMARY KEY (number))")

conn.commit()

conn.close()