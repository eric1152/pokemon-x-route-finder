#!/usr/bin/env python

import pokemon
from termcolor import colored

def capture(tpokemon):
	p = None
	p = pokemon.find_by_name(tpokemon)

	if p == None:
		p = pokemon.find_by_number(tpokemon)

	if p == None:
		print "Pokemon '" + pokemon + "' not found!"
	else:
		p.captured = True
		p.save()

		print colored('Captured ' + p.name + '!', 'green')

def release(tpokemon):
	p = None
	p = pokemon.find_by_name(tpokemon)

	if p == None:
		p = pokemon.find_by_number(tpokemon)

	if p == None:
		print "Pokemon '" + pokemon + "' not found!"
	else:
		p.captured = False
		p.save()

		print colored('Released ' + p.name + '!', 'green')

def search(tpokemon):
	p = None
	p = pokemon.find_by_name(tpokemon)

	if p == None:
		p = pokemon.find_by_number(tpokemon)

	if p == None:
		print colored("Pokemon '" + pokemon + "' not found!", 'red')
	else:
		print colored('#' + p.number + ' ' + p.name, 'cyan') + ': ' + p.ptype + ' type.'
		print 'Routes where this Pokemon is found: ' + colored(p.routes, 'cyan')
		print 'Egg group: ' + colored(p.eggGroup, 'cyan')

		if p.captured:
			print 'You ' + colored('have captured', 'green') + ' this Pokemon.'
		else:
			print 'You ' + colored('have not captured', 'red') + ' this Pokemon.'

def listall():
	for p in pokemon.all():
		print colored('#' + p.number + ' ' + p.name, 'cyan') + ': ' + p.ptype + ' type.'

def listFree():
	print 'You have not captured: '

	for p in pokemon.captured(False):
		print '    #' + p.number + ' ' + p.name

def captured():
	print 'You have captured: '

	for p in pokemon.captured(True):
		print '    #' + p.number + ' ' + p.name

def findRoute(route):
	r = pokemon.find_by_routes(route)

	if r == None:
		print colored('ERROR:', 'red') + 'no Pokemon found on that route.'
	else:
		for p in r:
			print '    #' + p.number + ' ' + p.name