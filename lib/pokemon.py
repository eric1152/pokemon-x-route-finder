#!/usr/bin/env python

import sqlite3

class Pokemon(object):
	"""Pokemon class, holds information about Pokemon and handles database saving"""
	def __init__(self, name = '', number = '', routes = '', ptype = '', eggGroup = '', captured = False):
		self.name = name
		self.number = number
		self.routes = routes
		self.ptype = ptype
		self.eggGroup = eggGroup
		self.captured = captured

	def save(self):
		conn = sqlite3.connect('db/pokemon.db')
		c = conn.cursor()

		if exists(self.number):
			c.execute("UPDATE pokemon SET name=?, routes=?, ptype=?, eggGroup=?, captured=? WHERE number=?", (self.name, self.routes, self.ptype, self.eggGroup, self.captured, self.number))
		else:
			c.execute("INSERT INTO pokemon VALUES (?, ?, ?, ?, ?, ?)", (self.name, self.number, self.routes, self.ptype, self.eggGroup, self.captured))

		conn.commit()

		conn.close()

def captured(state):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()
	r = None

	if state:
		state = 1
	else:
		state = 0

	c.execute("SELECT * FROM pokemon WHERE captured=?", (state,))
	r = c.fetchall()

	conn.close()

	if r == None:
		return r
	else:
		return map(convert_to_object, r)

def convert_to_object(item):
	return Pokemon(item[0], item[1], item[2], item[3], item[4], item[5])

def find_by_routes(route):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()
	r = None

	c.execute("SELECT * FROM pokemon WHERE routes LIKE '%?%'", (route,))
	r = c.fetchall()

	conn.close()

	if r == None:
		return r
	else:
		return map(convert_to_object, r)

def find_by_number(number):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()
	r = None

	c.execute("SELECT * FROM pokemon WHERE number=?", (number,))
	r = c.fetchone()

	conn.close()

	if r == None:
		return r
	else:
		return convert_to_object(r)

def find_by_name(name):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()
	r = None

	c.execute("SELECT * FROM pokemon WHERE name=?", (name,))
	r = c.fetchone()

	conn.close()

	if r == None:
		return r
	else:
		return convert_to_object(r)

def delete(number):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()

	c.execute("DELETE FROM pokemon WHERE number=?", (number,))

	conn.close()

def all():
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()

	results = []

	for row in c.execute("SELECT * FROM pokemon"):
		results.append(convert_to_object(row))

	conn.close()

	return results

def exists(number):
	conn = sqlite3.connect('db/pokemon.db')
	c = conn.cursor()
	result = None

	c.execute('SELECT * FROM pokemon WHERE number=?', (number,))
	result = c.fetchone()

	conn.close()

	if result is None:
		return False
	else:
		return True
