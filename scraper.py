#!/usr/bin/env python

#Scraper to gather pokemon data

import urllib2
from bs4 import BeautifulSoup
from lib.pokemon import Pokemon
import lib.pokemon

masterPage = BeautifulSoup(urllib2.urlopen('http://pokemondb.net/pokedex/game/x-y').read(), "lxml")

for entry in masterPage.find_all("a", class_="ent-name"):
	p = Pokemon()
	p.name = entry.string

	entryPage = BeautifulSoup(urllib2.urlopen('http://pokemondb.net' + entry['href']).read(), "lxml")
	p.number = entryPage.find("table", class_="vitals-table").find("td").string

	if "don't have any location data" not in entryPage.find_all(class_='colset')[-1].get_text():
		p.routes = entryPage.find_all(class_='colset')[-1].find_all("td")[-1].get_text()
	else:
		p.routes = ''

	p.ptype = entryPage.find_all("td")[1].get_text().replace("\n", "")

	p.eggGroup = entryPage.find_all("table")[2].td.get_text().replace("\n", "")

	p.save()

	print "Saved " + p.name