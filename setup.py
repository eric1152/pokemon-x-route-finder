#!/usr/bin/env python

from setuptools import setup

setup(name='routefinder',
	version='0.1',
	description='Pokemon X and Y route finder',
	url='https://bitbucket.org/eric1152/pokemon-x-route-finder',
	author='Eric Rawls',
	author_email='eric.rawls93@gmail.com',
	license='GPLv3',
	packages=['routefinder'],
	zip_safe=False
)