#!/usr/bin/env python

import sys
from lib.pokemon import Pokemon
import lib.pokemon
import lib.commands

args = sys.argv[1:]
command = args.pop(0)

if command == "capture":
	lib.commands.capture(args[0])
elif command == "release":
	lib.commands.release(args[0])
elif command == "search":
	lib.commands.search(args[0])
elif command == "list":
	lib.commands.listall()
elif command == "free":
	lib.commands.listFree()
elif command == "captured":
	lib.commands.captured()
elif command == "route":
	lib.commands.findRoute(args[0])
else:
	print """INVALID COMMAND! Valid commands are:

	capture [pokemon]
	release [pokemon]
	search [pokemon]
	route [route name]
	list
	free
	captured

where Pokemon is a valid Pokemon name or number"""